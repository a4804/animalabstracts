package com.mycompany.animalabstract;

public abstract class Vahicle {

    private String engine;

    public Vahicle(String engine) {
        this.engine = engine;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public abstract void startEngine();

    public abstract void stopEngine();

    public abstract void raiseSpeed();

    public abstract void applyBreak();

}
