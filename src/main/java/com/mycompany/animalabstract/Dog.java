package com.mycompany.animalabstract;

public class Dog extends LandAnimal {

    private String nickname;

    public Dog(String nickname) {
        super("Dog", 4);
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public void run() {
        System.out.println("Dog : " + nickname + " run");
    }

    @Override
    public void eat() {
        System.out.println("Dog : " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Dog : " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Dog : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Dog : " + nickname + " sleep");
    }

}
