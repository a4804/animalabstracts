package com.mycompany.animalabstract;

public class Crocodie extends Reptile {

    private String nickname;

    public Crocodie(String nickname) {
        super("Crocodie", 4);
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public void crawl() {
        System.out.println("Crocodie : " + nickname + " crawl");
    }

    @Override
    public void eat() {
        System.out.println("Crocodie : " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Crocodie : " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Crocodie : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodie : " + nickname + " sleep");
    }

}
