package com.mycompany.animalabstract;

public abstract class LandAnimal extends Animal implements Runable {

    public LandAnimal(String name, int numberOfLeg) {
        super(name, numberOfLeg);
    }

    public abstract void run();
}
