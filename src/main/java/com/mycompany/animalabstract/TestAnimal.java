package com.mycompany.animalabstract;

public class TestAnimal {

    public static void main(String[] args) {
        // Human h1 = new Human("Dang");
        // h1.eat();
        // h1.walk();
        // h1.run();
        // System.out.println("h1 is animal ? | " + (h1 instanceof Animal));
        // System.out.println("h1 is land animal ? | " + (h1 instanceof LandAnimal));
        // System.out.println("------------------------------------------");

        // Crocodie c1 = new Crocodie("Banana");
        // c1.eat();
        // c1.walk();
        // c1.crawl();
        // System.out.println("c1 is animal ? | " + (c1 instanceof Animal));
        // System.out.println("c1 is Reptile animal ? | " + (c1 instanceof Reptile));
        // System.out.println("------------------------------------------");
        // Snake s1 = new Snake("Basilus");
        // s1.eat();
        // s1.walk();
        // s1.crawl();
        // System.out.println("s1 is animal ? | " + (s1 instanceof Animal));
        // System.out.println("s1 is Reptile animal ? | " + (s1 instanceof Reptile));
        // System.out.println("------------------------------------------");
        // Cat cat1 = new Cat("Sphink");
        // cat1.eat();
        // cat1.walk();
        // cat1.run();
        // System.out.println("cat1 is animal ? | " + (cat1 instanceof Animal));
        // System.out.println("cat1 is land animal ? | " + (cat1 instanceof LandAnimal));
        // System.out.println("------------------------------------------");
        // Dog dog1 = new Dog("Anubis");
        // dog1.eat();
        // dog1.walk();
        // dog1.run();
        // System.out.println("dog1 is animal ? | " + (dog1 instanceof Animal));
        // System.out.println("dog1 is land animal ? | " + (dog1 instanceof LandAnimal));
        // System.out.println("------------------------------------------");
        // Fish f1 = new Fish("Magikarp");
        // f1.eat();
        // f1.walk();
        // f1.swim();
        // System.out.println("f1 is animal ? | " + (f1 instanceof Animal));
        // System.out.println("f1 is Aquatic animal ? | " + (f1 instanceof AquaticAnimal));
        // System.out.println("------------------------------------------");
        // Crab crab1 = new Crab("Magikarp");
        // crab1.eat();
        // crab1.walk();
        // crab1.swim();
        // System.out.println("crab1 is animal ? | " + (crab1 instanceof Animal));
        // System.out.println("crab1 is Aquatic animal ? | " + (crab1 instanceof AquaticAnimal));
        // System.out.println("------------------------------------------");
        // Bat b1 = new Bat("Vampire");
        // b1.eat();
        // b1.walk();
        // b1.fly();
        // System.out.println("b1 is animal ? | " + (b1 instanceof Animal));
        // System.out.println("b1 is Poulty ? | " + (b1 instanceof Poulty));
        // System.out.println("------------------------------------------");
        // Earthworm e1 = new Earthworm("Saiirung");
        // e1.eat();
        // e1.walk();
        // System.out.println("e1 is animal ? | " + (e1 instanceof Animal));
        // System.out.println("------------------------------------------");
        Bat bat = new Bat("Cypher");
        Plane plane = new Plane("Engine number I");
        bat.fly();
        plane.fly();
        Dog dog = new Dog("Jaja");
        Car car = new Car("Maxqueen");
        Submarine submarine = new Submarine("toylung");
        Snake snake = new Snake("Basillis");
        Crab crab = new Crab("Mr.Crab");

        Flyable[] flyable = {bat, plane};
        for (Flyable f : flyable) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }

        Runable[] runable = {dog, car};
        for (Runable r : runable) {
            if (r instanceof Car) {
                Car c = (Car) r;
                c.startEngine();
            }
            r.run();
        }

        Swimable[] swimable = {crab, submarine};
        for (Swimable s : swimable) {
            if (s instanceof Submarine) {
                Submarine su = (Submarine) s;
                su.startEngine();
            }
            s.swim();
        }

        Crawlable[] crawlable = {snake};
        for (Crawlable cr : crawlable) {
            cr.crawl();
        }

    }
}
