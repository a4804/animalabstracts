package com.mycompany.animalabstract;

public class Crab extends AquaticAnimal {

    private String nickname;

    public Crab(String nickname) {
        super("Crab");
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public void swim() {
        System.out.println("Crab : " + nickname + " swim");
    }

    @Override
    public void eat() {
        System.out.println("Crab : " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Crab : " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Crab : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crab : " + nickname + " sleep");
    }

}
