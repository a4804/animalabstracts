package com.mycompany.animalabstract;

public class Submarine extends Vahicle implements Swimable {

    private String engine;

    public Submarine(String engine) {
        super(engine);
        this.engine = engine;
    }

    @Override
    public void startEngine() {
        System.out.println("Submarine : " + engine + " start engine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Submarine : " + engine + " stop engine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Submarine : " + engine + " raise speed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Submarine : " + engine + " apply break");
    }

    @Override
    public void swim() {
        System.out.println("Submarine : " + engine + " swim ");
    }
}
