package com.mycompany.animalabstract;

public class Bat extends Poulty {

    private String nickname;

    public Bat(String nickname) {
        super("Bat", 2);
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public void fly() {
        System.out.println("Bat : " + nickname + " fly");
    }

    @Override
    public void eat() {
        System.out.println("Bat : " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Bat : " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Bat : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Bat : " + nickname + " sleep");
    }

}
