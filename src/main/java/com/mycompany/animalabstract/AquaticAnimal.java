package com.mycompany.animalabstract;

public abstract class AquaticAnimal extends Animal implements Swimable {

    public AquaticAnimal(String name) {
        super(name, 0);
    }

    public abstract void swim();
}
