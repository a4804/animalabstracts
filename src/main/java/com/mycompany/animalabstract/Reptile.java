package com.mycompany.animalabstract;

public abstract class Reptile extends Animal implements Crawlable {

    public Reptile(String name, int numberOfLeg) {
        super(name, numberOfLeg);
    }

    public abstract void crawl();
}
