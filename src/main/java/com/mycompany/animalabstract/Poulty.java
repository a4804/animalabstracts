package com.mycompany.animalabstract;

public abstract class Poulty extends Animal implements Flyable {

    public Poulty(String name, int numberOfLeg) {
        super(name, numberOfLeg);
    }

}
