package com.mycompany.animalabstract;

public class Snake extends Reptile {

    private String nickname;

    public Snake(String nickname) {
        super("Snake", 4);
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public void crawl() {
        System.out.println("Snake : " + nickname + " crawl");
    }

    @Override
    public void eat() {
        System.out.println("Snake : " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Snake : " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Snake : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Snake : " + nickname + " sleep");
    }

}
