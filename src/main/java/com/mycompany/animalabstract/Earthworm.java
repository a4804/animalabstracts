package com.mycompany.animalabstract;

public class Earthworm extends Animal {

    private String nickname;

    public Earthworm(String nickname) {
        super("Earthworm", 0);
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public void eat() {
        System.out.println("Earthworm : " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Earthworm : " + nickname + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Earthworm : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Earthworm : " + nickname + " sleep");
    }

}
